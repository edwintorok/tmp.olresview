module Static = struct
  type t = {
    b: int array;
    pattern_length: int;
  }

  let compile pattern =
    let pattern_length = String.length pattern in
    assert (pattern_length < 30);
    let b = Array.init 256 (fun _ -> lnot 0) in
    for i = 0 to pattern_length - 1 do
      let c = Char.code pattern.[i] in
      b.(c) <- b.(c) land (lnot (1 lsl i))
    done;
    { b; pattern_length }

  let rec shiftor_loop ~b ~mm ~m ~str ~n ~d i =
    if i < n then
      let mask = Array.unsafe_get b (Char.code (String.unsafe_get str i)) in
      let d = (d lsl 1) lor mask in
      if mask <> lnot 0 then
        if d land mm = mm then
          shiftor_loop ~b ~mm ~m ~str ~n ~d (i+1)
        else
          Some i (* report match *)
      else
        filter_try_skip ~b ~mm ~m ~str ~n i
    else
      None (* match failed *)

  and filter_try_skip ~b ~mm ~m ~str ~n i =
    let next_i = i + m in
    if next_i < n &&
       (Array.unsafe_get b (Char.code (String.unsafe_get str next_i)) = lnot 0) then
      filter_try_skip ~b ~mm ~m ~str ~n next_i
    else
      shiftor_loop ~b ~mm ~m ~str ~n ~d:(lnot 0) (i+1)

  let exec ~pos_start ~pos_end s str =
    if pos_end > String.length str then
      invalid_arg "pos_end out of bounds";
    let m = s.pattern_length in
    let mm = 1 lsl (m - 1) in
    let d = lnot 0 in
    shiftor_loop ~b:s.b ~mm ~m ~str ~n:pos_end ~d pos_start
end

let () =
  let f = open_in "/tmp/foo" in
  let siz = in_channel_length f in
  let str = String.create siz in
  really_input f str 0 siz;
  close_in f;
  let c = Static.compile "Printf" in
  let t0 = Unix.gettimeofday () in
  ignore (Static.exec ~pos_start:0 ~pos_end:(String.length str) c str);
  let t1 = Unix.gettimeofday () in
  Printf.printf "time: %f\n%!" (t1 -. t0)

