let verify = ref 0

let q = 6

let rec filter_search_loop b mm buf n d i =
  if i < n then
    let c0 = Char.code (String.unsafe_get buf (i))
    and c1 = Char.code (String.unsafe_get buf (i+q))
    and c2 = Char.code (String.unsafe_get buf (i+2*q))
    and c3 = Char.code (String.unsafe_get buf (i+3*q)) in
    let d = (d lsl 1) lor c0 in
    let d = (d lsl 1) lor c1 in
    let d = (d lsl 1) lor c2 in
    let d = (d lsl 1) lor c3 in
    if d land mm <> mm then
      incr verify;
    let d = d land (lnot mm) in
    filter_search_loop b mm buf n d (i + 4 * q)

let filter_search pattern buf =
  let b = Array.init 256 (fun _ -> lnot 0) in
  let u = 4 in
  let h = ref 0 and mm = ref 0 in
  for j = 0 to q - 1 do
    for i = 0 to String.length pattern / q - 1 do
      let p_iq_j = Char.code pattern.[i * q + j] in
      b.(p_iq_j) <- b.(p_iq_j) land (lnot (1 lsl !h));
      incr h
    done;
    for r = 0 to u - 1 do
      mm := !mm lor (1 lsl (!h-1));
      incr h
    done;
    decr h
  done;
  Array.iteri (fun i bi ->
    b.(i) <- bi land (lnot (!mm land (!mm lsl 1)))
  ) b;
  let d = lnot !mm in
  let t0 = Unix.gettimeofday () in
  filter_search_loop b !mm buf (String.length buf) d 0;
  let t1 = Unix.gettimeofday () in
  Printf.printf "verify: %d, %f\n" !verify (t1 -. t0)

let re_search pattern buf =
  let compiled = Re_perl.compile_pat pattern in
  let t0 = Unix.gettimeofday () in
  ignore (Re.execp compiled buf);
  let t1 = Unix.gettimeofday () in
  Printf.printf "re: %f\n" (t1 -. t0)

let () =
  let f = open_in "/tmp/foo" in
  let siz = in_channel_length f in
  let str = String.create siz in
  really_input f str 0 siz;
  close_in f;
  filter_search "Printf" str
(*  re_search "Printf" str*)
