let verify = ref 0

let q = 6

let rec filter_search_loop b mm buf n d i =
  if i < n then
    let c0 = Char.code (String.unsafe_get buf (i))
    and c1 = Char.code (String.unsafe_get buf (i+q))
    and c2 = Char.code (String.unsafe_get buf (i+2*q))
    and c3 = Char.code (String.unsafe_get buf (i+3*q)) in
    let d = (d lsl 1) lor c0 in
    let d = (d lsl 1) lor c1 in
    let d = (d lsl 1) lor c2 in
    let d = (d lsl 1) lor c3 in
    if d land mm <> mm then
      incr verify;
    let d = d land (lnot mm) in
    filter_search_loop b mm buf n d (i + 4 * q)

let filter_search pattern buf =
  let b = Array.init 256 (fun _ -> lnot 0) in
  let u = 4 in
  let h = ref 0 and mm = ref 0 in
  for j = 0 to q - 1 do
    for i = 0 to String.length pattern / q - 1 do
      let p_iq_j = Char.code pattern.[i * q + j] in
      b.(p_iq_j) <- b.(p_iq_j) land (lnot (1 lsl !h));
      incr h
    done;
    for r = 0 to u - 1 do
      mm := !mm lor (1 lsl (!h-1));
      incr h
    done;
    decr h
  done;
  Array.iteri (fun i bi ->
    b.(i) <- bi land (lnot (!mm land (!mm lsl 1)))
  ) b;
  let d = lnot !mm in
  let t0 = Unix.gettimeofday () in
  filter_search_loop b !mm buf (String.length buf) d 0;
  let t1 = Unix.gettimeofday () in
  Printf.printf "verify: %d, %f\n" !verify (t1 -. t0)

let re_search pattern buf =
  let compiled = Re_perl.compile_pat pattern in
  let t0 = Unix.gettimeofday () in
  ignore (Re.execp compiled buf);
  let t1 = Unix.gettimeofday () in
  Printf.printf "re: %f\n" (t1 -. t0)

let agrep_search pattern buf =
  let compiled = Agrep.pattern_string pattern in
  let t0 = Unix.gettimeofday () in
  ignore (Agrep.string_match compiled buf);
  let t1 = Unix.gettimeofday () in
  Printf.printf "agrep: %f\n" (t1 -. t0)

  (* normal shift-or, detect impossible character, and shift!
   * w = 30
   * u = 2
   * *)

let verif2 = ref 0
let rec filter2_rec_loop b init m buf d i =
  (* TODO: last char out of bounds! *)
  if i < String.length buf then
    let c0 = Char.code (String.unsafe_get buf i)
    and c1 = Char.code (String.unsafe_get buf (i+1)) in
    let d = (d lsl 2) lor ((Array.unsafe_get b c0) lsl 1) lor (Array.unsafe_get b c1) in
    if (lnot d) lsr 28 <> 0 then
      true
    else
    if d land init = init then begin
      filter2_rec_loop b init m buf d (i + 2*m)
    end
    else
      filter2_rec_loop b init m buf d (i + 2)
  else
    false

let filter2_search pattern buf =
  (* preprocess *)
  let m = min (String.length pattern) 30 in
  let init = ((1 lsl m) - 1) lsl (28 - m) in
  let b = Array.init 256 (fun _ -> init) in
  for i = 0 to m - 1 do
    let pi = Char.code pattern.[i] in
    b.(pi) <- b.(pi) land (lnot (1 lsl (28 - m + i)))
  done;
  let d = lnot 0 in
  let t0 = Unix.gettimeofday () in
  ignore (filter2_rec_loop b ((init lsl 1) lor init) m buf d 0);
  let t1 = Unix.gettimeofday () in
  Printf.printf "filter2: %f\n" (t1 -. t0)

let verif3 = ref 0

(* TODO: two loops of FAOSA:
  *  first loop: q = m -> "bad char" rule, check every m-th char, see if a
  *  pattern "fits" there
  *  whenever first loop says there can be a match I go back m-1 chars
  *  and try shift-or *)

let rec filter3_rec_loop b buf mm m d i =
  if i < String.length buf then
    let c = Char.code (String.unsafe_get buf i) in
    let b_c = Array.unsafe_get b c in
    let d = (d lsl 1) lor b_c in
    if d land mm <> mm then
      true
    else begin
      if d = lnot 0 then begin
        (* "bad char" rule: a match cannot start at this position *)
        filter3_rec_loop b buf mm m d (i + m)
      end
      else
        filter3_rec_loop b buf mm m d (i + 1)
    end
  else
    false

let filter3_search pattern buf =
  (* preprocess *)
  let m = min (String.length pattern) 30 in
  let b = Array.init 256 (fun _ -> lnot 0) in
  for i = 0 to m - 1 do
    let pi = Char.code pattern.[i] in
    b.(pi) <- b.(pi) land (lnot (1 lsl i))
  done;
  let d = lnot 0 in
  let mm = 1 lsl (m - 1) in
  let t0 = Unix.gettimeofday () in
  ignore (filter3_rec_loop b buf mm m d 0);
  let t1 = Unix.gettimeofday () in
  Printf.printf "filter3: %f\n" (t1 -. t0)

let verif4 = ref 0
let filter4_search pattern buf =
  (* preprocess *)
  let m = min (String.length pattern) 30 in
  let b = Array.init 256 (fun _ -> lnot 0) in
  for i = 0 to m - 1 do
    let pi = Char.code pattern.[i] in
    b.(pi) <- b.(pi) land (lnot (1 lsl i))
  done;
  let d = lnot 0 in
  let mm = 1 lsl (m - 1) in
  let t0 = Unix.gettimeofday () in
  let rec filter4_rec_loop d i =
    if i < String.length buf then
      let c = Char.code (String.unsafe_get buf i) in
      let d = (d lsl 1) lor (Array.unsafe_get b c) in
      if d land mm = mm then begin
        if d = lnot 0 then begin
          filter4_rec_loop d (i + m)
        end
        else
          filter4_rec_loop d (i + 1)
      end else incr verif4
  in
  filter4_rec_loop d 0;
  let t1 = Unix.gettimeofday () in
  Printf.printf "filter4: %f\n" (t1 -. t0)

let rec filter5_check_end ~b ~buf ~mm ~m ~n ~d i =
  if i < n then
    let mask = Array.unsafe_get b (Char.code (String.unsafe_get buf i)) in
    if mask = lnot 0 then
      (* the pattern cannot overlap this position: this character is not part of
       * the pattern, at best it can start matching starting from the next
       * position, so skip a whole pattern's length
       * *)
      filter5_check_end ~b ~buf ~mm ~m ~n ~d:(lnot 0) (i + m)
    else
      (* char is part of pattern: the pattern can end here, check from
         previous position until current one using shift-or *)
     filter5_check_shiftor ~b ~buf ~mm ~m ~d (i-m+1) ~n:i ~realn:n
  else
    None (* no match *)

and filter5_check_shiftor ~b ~buf ~mm ~m ~d ~realn ~n j =
  if j <= n then
    let mask = Array.unsafe_get b (Char.code (String.unsafe_get buf j)) in
    let d = (d lsl 1) lor mask in
    if d land mm = mm then
      filter5_check_shiftor ~b ~buf ~mm ~m ~d ~realn ~n (j+1)
    else
      Some j (* report match *)
  else
    filter5_check_end ~b ~buf ~mm ~m ~n:realn ~d j

let filter5_search pattern buf =
  (* preprocess *)
  let m = min (String.length pattern) 30 in
  let b = Array.init 256 (fun _ -> lnot 0) in
  for i = 0 to m - 1 do
    let pi = Char.code pattern.[i] in
    b.(pi) <- b.(pi) land (lnot (1 lsl i))
  done;
  let d = lnot 0 in
  let mm = 1 lsl (m - 1) in
  let t0 = Unix.gettimeofday () in
  ignore (filter5_check_end ~b ~buf ~mm ~m ~n:(String.length buf) ~d (m-1));
  let t1 = Unix.gettimeofday () in
  Printf.printf "filter5: %f\n" (t1 -. t0)

let filter6_search pattern buf =
  (* preprocess *)
  let m = min (String.length pattern) 30 in
  let b = Array.init 256 (fun _ -> lnot 0) in
  for i = 0 to m - 1 do
    let pi = Char.code pattern.[i] in
    b.(pi) <- b.(pi) land (lnot (1 lsl i))
  done;
  let d = lnot 0 in
  let mm = 1 lsl (m - 1) in
  let rec filter6_check_end d i =
    if i < String.length buf then
      let mask = Array.unsafe_get b (Char.code (String.unsafe_get buf i)) in
      if mask = lnot 0 then
        (* the pattern cannot overlap this position: this character is not part of
         * the pattern, at best it can start matching starting from the next
         * position, so skip a whole pattern's length
         * *)
        filter6_check_end (lnot 0) (i + m)
      else
        (* char is part of pattern: the pattern can end here, check from
           previous position until current one using shift-or *)
        filter6_check_shiftor d (i-m+1) i
    else
      None (* no match *)

  and filter6_check_shiftor d j n =
    if j <= n then
      let mask = Array.unsafe_get b (Char.code (String.unsafe_get buf j)) in
      let d = (d lsl 1) lor mask in
      if d land mm = mm then
        filter6_check_shiftor d (j+1) n
      else
        Some j (* report match *)
    else
      filter6_check_end d j
  in

  let t0 = Unix.gettimeofday () in
  ignore (filter6_check_end d (m-1));
  let t1 = Unix.gettimeofday () in
  Printf.printf "filter6: %f\n" (t1 -. t0)

let rec filter7_check b buf mm m mask d i =
  if i < String.length buf then
    (* TODO: bounds *)
    let mask_next = Array.unsafe_get b (Char.code (String.unsafe_get buf (i + m))) in
    if mask land mask_next = lnot 0 then
      filter7_check b buf mm m mask_next d (i + m)
    else
      let d = (d lsl 1) lor mask in
      let mask_next = Array.unsafe_get b (Char.code (String.unsafe_get buf (i + 1))) in
      if d land mm = mm then
        filter7_check b buf mm m mask_next d (i+1)
      else
        Some i
  else
    None

let filter7_search pattern buf =
  (* preprocess *)
  let m = min (String.length pattern) 30 in
  let b = Array.init 256 (fun _ -> lnot 0) in
  for i = 0 to m - 1 do
    let pi = Char.code pattern.[i] in
    b.(pi) <- b.(pi) land (lnot (1 lsl i))
  done;
  let d = lnot 0 in
  let mm = 1 lsl (m - 1) in
  let t0 = Unix.gettimeofday () in
  let mask = Array.unsafe_get b (Char.code (String.unsafe_get buf 0)) in
  ignore (filter7_check b buf mm m mask d 0);
  let t1 = Unix.gettimeofday () in
  Printf.printf "filter7: %f\n" (t1 -. t0)

let rec filter8_check ~b ~buf ~mm ~m ~n ~d i =
  if i < n then
    let mask = Array.unsafe_get b (Char.code (String.unsafe_get buf i)) in
    let d = (d lsl 1) lor mask in
    if mask > 0 then (* mask <> lnot 0  => mask <> -1 *)
      if d land mm = mm then
        filter8_check ~b ~buf ~mm ~m ~n ~d (i+1)
      else
        Some i (* report match *)
    else
      (* try to skip a full pattern length *)
      filter8_skip ~b ~buf ~mm ~m ~n (i+m)
  else
    None

and filter8_skip ~b ~buf ~mm ~m ~n i =
  if i < n  &&
     (Array.unsafe_get b (Char.code (String.unsafe_get buf i)) < 0) then
    filter8_skip ~b ~buf ~mm ~m ~n (i+m)
  else
    filter8_check ~b ~buf ~mm ~m ~n ~d:(lnot 0) (i-m+1)

let filter8_search pattern buf =
  (* preprocess *)
  let m = min (String.length pattern) 30 in
  let b = Array.init 256 (fun _ -> lnot 0) in
  for i = 0 to m - 1 do
    let pi = Char.code pattern.[i] in
    b.(pi) <- b.(pi) land (lnot (1 lsl i))
  done;
  let mm = 1 lsl (m - 1) in
  let t0 = Unix.gettimeofday () in
  ignore (filter8_check ~b ~buf ~mm ~m ~n:(String.length buf) ~d:(lnot 0) 0);
  let t1 = Unix.gettimeofday () in
  Printf.printf "filter8: %f\n" (t1 -. t0)

type state = {
  b: int array;(* masks for shift-or bitmachine *)
  buf: string;
  mm: int; (* end marker for shift-or bitmachine *)
  m: int; (* pattern length *)
}

let rec filter9_check s d i =
  if i < String.length s.buf then
    let mask = Array.unsafe_get s.b (Char.code (String.unsafe_get s.buf i)) in
    let d = (d lsl 1) lor mask in
    if mask > 0 then (* mask <> lnot 0  => mask <> -1 *)
      if d land s.mm = s.mm then
        filter9_check s d (i+1)
      else
        Some i (* report match *)
    else
      (* try to skip a full pattern length *)
      filter9_skip s (i+s.m)
  else
    None

and filter9_skip s i =
  if i < String.length s.buf  &&
     (Array.unsafe_get s.b (Char.code (String.unsafe_get s.buf i)) < 0) then
    filter9_skip s (i+s.m)
  else
    filter9_check s (lnot 0) (i-s.m+1)

let filter9_search pattern buf =
  (* preprocess *)
  let m = min (String.length pattern) 30 in
  let b = Array.init 256 (fun _ -> lnot 0) in
  for i = 0 to m - 1 do
    let pi = Char.code pattern.[i] in
    b.(pi) <- b.(pi) land (lnot (1 lsl i))
  done;
  let d = lnot 0 in
  let mm = 1 lsl (m - 1) in
  let t0 = Unix.gettimeofday () in
  ignore (filter9_check {b;buf;mm; m} d 0);
  let t1 = Unix.gettimeofday () in
  Printf.printf "filter9: %f\n" (t1 -. t0)

let rec filter10_check_end ~b ~buf ~mm ~m ~n ~d i =
  if i < n then
    let mask = Array.unsafe_get b (Char.code (String.unsafe_get buf i)) in
    if mask = lnot 0 then
      (* the pattern cannot overlap this position: this character is not part of
       * the pattern, at best it can start matching starting from the next
       * position, so skip a whole pattern's length
       * *)
      filter10_check_end ~b ~buf ~mm ~m ~n ~d:(lnot 0) (i + m)
    else
      (* char is part of pattern: the pattern can end here, check from
         previous position until current one using shift-or *)
     filter10_check_shiftor ~b ~buf ~mm ~m ~d (i-m+1) ~n:i ~realn:n
  else
    None (* no match *)

and filter10_check_shiftor ~b ~buf ~mm ~m ~d ~realn ~n j =
  if j <= n then
    let mask = Array.unsafe_get b (Char.code (String.unsafe_get buf j)) in
    let d = (d lsl 1) lor mask in
    if d land mm = mm then
      filter10_check_shiftor ~b ~buf ~mm ~m ~d ~realn ~n (j+1)
    else
      Some j (* report match *)
  else
    filter10_check_end ~b ~buf ~mm ~m ~n:realn ~d j

let filter10_search pattern buf =
  (* preprocess *)
  let m = min (String.length pattern) 30 in
  let b = Array.init 256 (fun _ -> lnot 0) in
  for i = 0 to m - 1 do
    let pi = Char.code pattern.[i] in
    b.(pi) <- b.(pi) land (lnot (1 lsl i))
  done;
  let d = lnot 0 in
  let mm = 1 lsl (m - 1) in
  let t0 = Unix.gettimeofday () in
  ignore (filter10_check_end ~b ~buf ~mm ~m ~n:(String.length buf) ~d (m-1));
  let t1 = Unix.gettimeofday () in
  Printf.printf "filter10: %f\n" (t1 -. t0)

let () =
  let f = open_in "/tmp/foo" in
  let siz = in_channel_length f in
  let str = String.create siz in
  really_input f str 0 siz;
  close_in f;
  filter_search "Printf" str;
  re_search "Printf" str;
  agrep_search "Printf" str
  filter2_search "Printf" str;
  filter3_search "Printf" str;
  filter4_search "Printf" str;
  filter5_search "Printf" str;
  filter7_search "Printf" str;
  filter8_search "Printf" str;
  filter9_search "Printf" str;
  filter10_search "Printf" str
