module Static : sig
  type t
  val compile: string -> t
  val exec: ?pos_start:int -> ?pos_end:int -> t -> string -> int option
end
